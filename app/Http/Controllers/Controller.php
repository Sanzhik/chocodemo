<?php

namespace App\Http\Controllers;


use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View as View;
use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Stock;

class table extends Eloquent {
 
}
class Controller extends BaseController
{
      public function index(){
         $tables = table::paginate(2);;

        if (request() -> has('sort')){

        	$tables = table::orderby('title', request('sort'))->paginate(1);
        }

        if (request() -> has('sortprice')){

        	$tables = table::orderby('price', request('sortprice'))->paginate(2);
        }

        return view('index')->with('tables',$tables);


    }


    
}

