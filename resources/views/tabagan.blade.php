@extends('layouts.default');
@section('content')
 <div class="container">
      <div class="content">
        <div class="row">
          <div class="top-txt-bar">
            <div class="col-md-6">
              <p>Можно купить с 22 декабря по 15 февраля</p>
            </div>
            <div class="col-md-6 text-right">
              <p>Можно воспользоваться до 28 февраля 2018 года</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 main-block">
            <h3>Экстремально круто! Абсолютно новый снежный склон ждет вас! Катания на лыжах и сноуборде для взрослых и детей со скидкой до 30% в СРК Табаган!</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 images">
            <img src="img/tabagan.jpg" alt="">
          </div>
          <div class="col-md-4 information text-center">
            <h4>от 1 400тг.</h4>
            <p>Экономия <span>от 600тг</span></p>
            <input type="submit" value="Купить">
            <h1>Купили 2465 человек</h1><hr>
             <p>До завершения акции осталось:</p>
             <div class="time">
               <i class="far fa-clock"></i><span>08:32:09</span>
             </div><hr>
          </div>
        </div>
      </div>
    

    <div class="last-block">
      <div class="container">
        <div class="container">
          <div class="row">
              <div class="col-md-12 ">
                <div class="nav-elements">
                <div class="tabs">
                    <span class="tab active-tab">Информация</span>
                    <span class="tab">Отзывы</span>
                    <span class="tab">Вопросы</span>
                    <span class="tab">Получить деньги</span>
                </div>
                </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12 text-right">
                <div class="tab_content">
                    <div class="tab_item">
                      <div class="info">
                        <div class="col-md-6 left-block">
                          <div class="buy">
                            <img src="img/buy.png" alt="">
                          </div>
                          <div class="main-txt">
                            <h1>Условия:</h1>
                            <div class="certificate">
                              <span>•</span>  <span style="font-size:14px;">Сертификат предоставляет возможность посетить спортивно-развлекательный комплекс Табаган.</span>
                              <h5>Виды сертификатов</h5>
                              <h3>Абонементы на новый склон:</h3>
                              <p>Дневной абонемент на взрослого (в выходные) — 4 500 тг. вместо 6 000 тг.</p><a href="#">Купить</a>
                              <p>Дневной абонемент на взрослого (в выходные) — 2 100 тг. вместо 3 000 тг.</p><a href="#">Купить</a>
                              <p>Дневной абонемент на взрослого (в выходные) — 4 500 тг. вместо 6 000 тг.</p><a href="#">Купить</a>
                              <p>Дневной абонемент на взрослого (в выходные) — 2 100 тг. вместо 3 000 тг.</p><a href="#">Купить</a>
                              <p>Дневной абонемент на взрослого (в выходные) — 4 500 тг. вместо 6 000 тг.</p><a href="#">Купить</a>
                              <h3>Абонементы на старый склон:</h3>
                              <p>Дневной абонемент на взрослого (в выходные) — 4 500 тг. вместо 6 000 тг. (предложение закончилось)</p>
                              <p>Дневной абонемент на взрослого (в выходные) — 4 500 тг. вместо 6 000 тг. (предложение закончилось)</p>
                              <p>Дневной абонемент на взрослого (в выходные) — 4 500 тг. вместо 6 000 тг. (предложение закончилось)</p>
                              <p>Дневной абонемент на взрослого (в выходные) — 4 500 тг. вместо 6 000 тг. (предложение закончилось)</p>
                              <p>Дневной абонемент на взрослого (в выходные) — 4 500 тг. вместо 6 000 тг. (предложение закончилось)</p>
                              <span>•</span> <span style="font-size:14px;font-weight:bold; opacity:0.8;">Справки по телефонам:</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 right-block">
                          <div class="map">
                            <img src="img/map.png" alt="">
                          </div>
                          <iframe width="450" height="300" src="https://www.youtube.com/embed/CIMOwbK8axo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                      </div>
                      </div>
                    <div class="tab_item">
                      <div class="info"></div>
                    </div>
                    <div class="tab_item">
                      <div class="info"></div>
                    </div>
                    <div class="tab_item">
                      <div class="info"></div>
                    </div>
                    </div>
              </div>
              </div>
        </div>
      </div>
    </div>

  </div>
@stop