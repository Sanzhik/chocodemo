<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Купоны, скидки и акции в Казахстане: все скидочные купоны в одном месте на сайте Chocolife.me</title>
    <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{ URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') }} "></script>

    <script defer src="{{ URL::asset('https://use.fontawesome.com/releases/v5.0.6/js/all.js') }}"></script>
  </head>
  <body>
  <div id="top-bar">
    <div class="container">
      <div class="row">
        <div class="col-md-6 sections">
          <ul>
            <li><a href="#">Chocomart</a></li>
            <li><a href="#">Chocotravel</a></li>
            <li><a href="#">Lensmark</a></li>
            <li><a href="#">Chocofood</a></li>
            <li><a href="#">IDoctor</a></li>
          </ul>
        </div>
        <div class="col-md-6 auth-btn">
          <ul>
            <li><a href="#" class="sign-up">Регистрация</a></li>
            <li><a href="#">Вход</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div id="second-bar">
    <div class="container">
      <div class="row scn-br">
        <div class="col-md-3 city">
          <select>
            <option>Алматы</option>
            <option>Астана</option>
			<option>Шымкент</option>
            <option>Актау</option>
            <option>Актобе</option>
			<option>Атырау</option>
			<option>Балхаш</option>
			<option>Жезказган</option>
			<option>Караганда</option>
			<option>Кокшетау</option>
          </select>
        </div>
        <div class="col-md-3 assist">
          <h5><i class="icon-tint"></i>Нужна помощь?</h5>
        </div>
        <div class="col-md-3 secr">
          <h5>Защита покупателей</h5>
        </div>
        <div class="col-md-3 call">
          <h5>Обратная связь</h5>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 logo">
          <img src="img/logo.png" alt="">
        </div>
        <div class="search-nav">
          <div class="col-md-8 search">
            <input type="text" placeholder="Найти 603 акции" class="search-bar">
            <input type="button" name="" class="search-btn">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 banner ">
          <img src="img/blackstar.jpg" alt="">
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <nav class="nav">
           <ul class="nav navbar-nav navbar-left">
               <li class="nav-item"><a href="#" title="">Все</a></li>
               <li class="nav-item"><a href="#" title="">Новые</a></li>
               <li class="nav-item"><a href="#" title="">Хиты продаж</a></li>
               <li class="nav-item"><a href="#" title="" class="dropdown" data-toggle="dropdown">Развлечения и отдых</a>
                 <ul class="dropdown-menu">
                     <li><a href="#">Активный отдых</a></li>
                     <li><a href="#">Бассейны</a></li>
                     <li><a href="#">Караоке</a></li>
                     <li><a href="#">Развлечения для детей</a></li>
                     <li><a href="#">Парки развлечений</a></li>
                     <li><a href="#">Кинотеатры</a></li>
                     <li><a href="#">Концерты и события</a></li>
                     <li><a href="#">Театр им. Лермонтова</a></li>
                     <li><a href="#">Квесты, игровые клубы</a></li>
                     <li><a href="#">Сауна, бани</a></li>
                 </ul>
                   </li>
              <li class="nav-item"><a href="#" title="" class="dropdown" data-toggle="dropdown">Красота и здаровье</a>
                 <ul class="dropdown-menu">
                     <li><a href="#">Активный отдых</a></li>
                     <li><a href="#">Бассейны</a></li>
                     <li><a href="#">Караоке</a></li>
                     <li><a href="#">Развлечения для детей</a></li>
                     <li><a href="#">Парки развлечений</a></li>
                     <li><a href="#">Кинотеатры</a></li>
                     <li><a href="#">Концерты и события</a></li>
                     <li><a href="#">Театр им. Лермонтова</a></li>
                     <li><a href="#">Квесты, игровые клубы</a></li>
                     <li><a href="#">Сауна, бани</a></li>
                 </ul>
                   </li>

               <li class="nav-item"><a href="#" title="" class="dropdown" data-toggle="dropdown">Спорт</a>
    		      <ul class="dropdown-menu">
    		         <li><a href="#">Тренажерный зал</a></li>
                     <li><a href="#">Танцы, фитнес</a></li>
                     <li><a href="#">Йога</a></li>
                     <li><a href="#">Единоборства</a></li>
                     <li><a href="#">Прокат и аренда</a></li>
    			  </ul>
    		   </li>

               <li class="nav-item"><a href="#" title="" class="dropdown" data-toggle="dropdown">Товары</a>
    		      <ul class="dropdown-menu">
    			     <li><a href="#">Одежда и аксессуары</a></li>
                     <li><a href="#">Товары для детей</a></li>
                     <li><a href="#">Цветы и подарки</a></li>
                     <li><a href="#">Товары для здаровья</a></li>
                     <li><a href="#">Электроника</a></li>
    				 <li><a href="#">Товары для дома и сада</a></li>
    			  </ul>
    		   </li>

               <li class="nav-item"><a href="#" title="" class="dropdown" data-toggle="dropdown">Услуги</a>
    		      <ul class="dropdown-menu">
    			     <li><a href="#">Химчистка и клининг</a></li>
                     <li><a href="#">Обучающие курсы</a></li>
                     <li><a href="#">Развивающие курсы для детей</a></li>
                     <li><a href="#">Языковые курсы</a></li>
                     <li><a href="#">Фото, видео</a></li>
    				 <li><a href="#">Автоуслуги</a></li>
    				 <li><a href="#">Поможем вместе</a></li>
    				 <li><a href="#">Другие услуги</a></li>
    			  </ul>
    		   </li>

               <li class="nav-item"><a href="#" title="" class="dropdown" data-toggle="dropdown">Еда</a>
    		      <ul class="dropdown-menu">
    			  	 <li><a href="#">Рестораны, кафе и пабы</a></li>
                     <li><a href="#">Доставка</a></li>
                     <li><a href="#">Продукты питания</a></li>
                     <li><a href="#">Банкеты</a></li>
                     <li><a href="#">Сеты</a></li>
    			  </ul>
    		   </li>

               <li class="nav-item"><a href="#" title="" class="dropdown" data-toggle="dropdown">Туризм, отели</a>
    		   	  <ul class="dropdown-menu">
    			     <li><a href="#">Санатории</a></li>
                     <li><a href="#">Гостиницы, отелм</a></li>
                     <li><a href="#">Внутренний туризм</a></li>
                     <li><a href="#">Отдых за границей</a></li>
                     <li><a href="#">Авиа, авто, ж/д, билеты</a></li>
    				 <li><a href="#">Боровое</a></li>
    			  </ul>
    		   </li>
           </ul>
      </nav>
      </div>
    </div>
  </div>
      <div class="container sect">
    <div class="row">
      <div class="col-md-3"> <img src="img/1a.jpg" alt="" class="board"></div>
      <div class="col-md-3"> <img src="img/2a.jpg" alt="" class="board"></div>
      <div class="col-md-3"> <img src="img/3a.jpg" alt="" class="board"></div>
      <div class="col-md-3"> <img src="img/4a.jpg" alt="" class="board"></div>
    </div>
  </div>
       <div class="container">
            <div class="row">
              <div class="col-md-12 table-sort text-center">
                <ul>
                  <li><span>Сортировать:</span></li>
                  <li><a href="/?sort=asc"><i class="fas fa-dot-circle"></i>Название</a></li>   
                  <li><a href="/?sortprice=asc" class="active"><i class="fas fa-dot-circle"></i>Цена</a></li>
                  <li><a href="#"><i class="fas fa-dot-circle"></i> Скидка</a></li>
                  <li><a href="#"><i class="fas fa-dot-circle"></i> Новые</a></li>
                  <li><a href="#"><i class="fas fa-dot-circle"></i> Рейтинг</a></li>
                </ul>
              </div>
            </div>
        </div>